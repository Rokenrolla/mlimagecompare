﻿using Microsoft.ML;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace MLFlowersImgComp.Models
{
    public class ImageResult
    {
        public string Image { get; set; }
        public float Rose { get; set; }
        public float Violet { get; set; }
        public float Tulip { get; set; }
    }
}
