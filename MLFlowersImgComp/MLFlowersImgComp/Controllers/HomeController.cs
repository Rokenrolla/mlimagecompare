﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting.Internal;
using Microsoft.Extensions.Logging;
using MLFlowersImgComp.Models;
using MLFlowersImgCompML.Model;

namespace MLFlowersImgComp.Controllers
{
    public class HomeController : Controller
    {
        public static string name1;
        public IActionResult Index()
        {
            if (name1 == null)
            {
                return View();
            }
            else
            {
                ImageResult imageResult = new ImageResult();
                var input = new ModelInput();
                input.ImageSource = @"wwwroot\TestingImages\" + name1;
                ModelOutput result = ConsumeModel.Predict(input);
                imageResult.Rose = result.Score[0] * 100;
                imageResult.Tulip = result.Score[1] * 100;
                imageResult.Violet = result.Score[2] * 100;
                imageResult.Image = "/TestingImages/" + name1;
                return View(imageResult);
            }
            
        }

        

        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return Content("file not selected");

            var path = Path.Combine(
                        Directory.GetCurrentDirectory(), "wwwroot/TestingImages",
                        file.FileName);
            name1 = file.FileName;

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);

            }            
            return RedirectToAction("Index");
        }



    }
}
